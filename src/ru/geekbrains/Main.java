package ru.geekbrains;

public class Main {

    public static void main(String[] args) {
        //task1();
        task2();
    }

    /**
     * Задача 1
     * Реализовать простейшую хэш-функцию. На вход функции подается строка, на выходе сумма кодов символов.
     */
    public static long task1(String str) {
        long hash = 0;
        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            hash += c;
        }
        return hash;
    }

    /**
     * Задача 2
     * Переписать программу, реализующее двоичное дерево поиска.
     *   а) Добавить в него обход дерева различными способами;
     *   б) Реализовать поиск в двоичном дереве поиска;
     *   в) *Добавить в программу обработку командной строки с помощью которой можно указывать
     *      из какого файла считывать данные, каким образом обходить дерево.
     */
    public static void task2() {
        int rootData = 20;
        BinaryTree bTree = new BinaryTree();
        for (int i = 0; i < 20; i++) {
            int num = (int) (Math.random() * rootData);
            bTree.insert(num);
        }

        // а) Добавить в него обход дерева различными способами;
        System.out.println("Обход в прямом порядке:");
        bTree.printPreOrder();
        System.out.println("");

        System.out.println("Симметричный обход:");
        bTree.printInOrder();
        System.out.println("");

        System.out.println("Обход в обратном порядке:");
        bTree.printPostOrder();
        System.out.println("");

        // б) Реализовать поиск в двоичном дереве поиска;
        int search = 45;
        if (bTree.search(search) == null) System.out.println("Значение " + search + " в двоичном дереве не найдено!");
        else System.out.println("Значение " + search + " в двоичном дереве найдено!");
        // Печать двоичного дерева
        bTree.print();
    }
}
