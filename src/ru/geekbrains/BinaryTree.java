package ru.geekbrains;

public class BinaryTree {

    private TreeNode root;

    public BinaryTree() {
        root = null;
    }

    public void insert(int ...nodeData) {
        for (int i = 0; i < nodeData.length; i++) {
            int data = nodeData[i];
            if (root == null) root = new TreeNode(data);
            else insert(root, data);
        }
    }

    private void insert(TreeNode node, int data) {
        // Слева от каждого узла находятся узлы, ключи которых меньше или равны ключу данного узла.
        if (data <= node.getData()){
            if (node.getLeft() != null) insert(node.getLeft(), data);
            else node.setLeft(new TreeNode(data));
            return;
        }
        // Справа от каждого узла находятся узлы, ключи которых больше данного узла.
        if (node.getRight() != null) insert(node.getRight(), data);
        else node.setRight(new TreeNode(data));
    }

    public void print() {
        root.print();
    }

    public TreeNode search(int data) {
        return search(root, data);
    }

    public TreeNode search(TreeNode root, int data) {
        if (root == null || root.getData() == data) return root;
        if (root.getData() > data) return search(root.getLeft(), data);
        return search(root.getRight(), data);
    }

    public void printPostOrder() {
        printPostOrder(root);
    }

    private void printPostOrder(TreeNode node) {
        if (node == null) return;
        printPostOrder(node.getLeft());
        printPostOrder(node.getRight());
        System.out.print(node.getData() + " ");
    }

    public void printInOrder() {
        printInOrder(root);
    }

    private void printInOrder(TreeNode node) {
        if (node == null) return;
        printInOrder(node.getLeft());
        System.out.print(node.getData() + " ");
        printInOrder(node.getRight());
    }

    public void printPreOrder() {
        printPreOrder(root);
    }

    private void printPreOrder(TreeNode node) {
        if (node == null) return;
        System.out.print(node.getData() + " ");
        printPreOrder(node.getLeft());
        printPreOrder(node.getRight());
    }
}
