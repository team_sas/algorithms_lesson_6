package ru.geekbrains;

public class TreeNode {
    private TreeNode left;
    private TreeNode right;
    private int data;

    public TreeNode(int data){
        left = null;
        right = null;
        this.data = data;
    }

    public void setData(int data) {
        this.data = data;
    }

    public void setLeft(TreeNode left) {
        this.left = left;
    }

    public void setRight(TreeNode right) {
        this.right = right;
    }

    public int getData() {
        return data;
    }

    public TreeNode getLeft() {
        return left;
    }

    public TreeNode getRight() {
        return right;
    }

    public void print() {
        print("", this, true);
    }

    /**
     * Печать дерева не реализовывал самостоятельно, скопировал первый попавшийся вариант из StackOverflow.
     * Единственное что изменил, это исправил ошибку связанную с тем, что дерево печаталось зеркально,
     * тоесть слева находились ноды с большим значением, вместо нод с меньшим значением.
     */
    public void print(String prefix, TreeNode n, boolean isLeft) {
        if (n != null) {
            System.out.println(prefix + (isLeft ? "|-- " : "\\-- ") + n.data);
            print(prefix + (isLeft ? "|   " : "    "), n.right, true);
            print(prefix + (isLeft ? "|   " : "    "), n.left, false);
        }
    }
}
